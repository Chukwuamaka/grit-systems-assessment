//Solution 1
function triplets(S) {
	let array = [];
	let item = [];
	for (let i = 0; i <= 0; i++) {
		for (let j = i+1; j < S.length; j++) {
			for (let k = j+1; k < S.length; k++) {
				let sum = S[i]+S[j]+S[k];
				if (sum === 0) {
					item = [S[i], S[j], S[k]];
					// item.sort(function(a,b) {
					// 	return a-b;
					// });
					array.push(item);
				}
			}
		}
	}
	return array;
}
triplets([-1,0,1,2,-1,4]);


// Solution 2
function triplets(S) {
	let array = [];
	let item = [];
	for (let i = 0; i <= 0; i++) {
		for (let j = 1; j < S.length; j++) {
			for (let k = 2; k < S.length; k++) {
				let sum = S[i]+S[j]+S[k];
				if (sum === 0) {
					item = [S[i], S[j], S[k]];
					item.sort(function(a,b) {
						return a-b;
					});
					array.push(item);
				}
			}
		}
	}
	for (let m = 0; m < array.length; m++) {
		for (let n = m+1; n < array.length; n++) {
			if((array[m][0] === array[n][0]) && (array[m][1] === array[n][1]) && (array[m][2] === array[n][2])) {
				array.splice(n, 1);
        	}
		}
	}
	return array;
}
triplets([-1,0,1,2,-1,4]);